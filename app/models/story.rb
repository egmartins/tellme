class Story < ActiveRecord::Base

	belongs_to :user
	has_many :comments, :dependent => :delete_all
	acts_as_taggable
	acts_as_votable
	validates :title, presence: true
	validates :content, presence: true
	validates :tag_list, presence: true

end
