class StoriesController < ApplicationController
	layout 'application'

	before_action :find_story, only: [:show, :edit, :update, :destroy, :upvote, :downvote]
	before_action :authenticate_user!


	def index
		if params[:tag]
			@stories = Story.all.order("created_at DESC").tagged_with(params[:tag])
		else
			@stories = Story.all.order("created_at DESC")
		end

	end

	def show
		
	end

	def new
		@story = current_user.stories.build
	end

	def create
		@story = current_user.stories.build(story_params)
		if @story.save
			redirect_to @story
		else
			render 'new'
		end
	end

	def edit
		
	end

	def update
		if @story.update(story_params)
			redirect_to @story
		else
			render 'edit'
		end
	end

	def destroy
		@story.destroy
		redirect_to stories_path
	end

	def upvote
		@story.upvote_by current_user
		redirect_to :back
	end

	def downvote
		@story.downvote_by current_user
		redirect_to :back
	end


private
	
	def find_story
		@story = Story.find(params[:id])
	end

	def story_params
		params.require(:story).permit(:title, :tag_list, :content, :user_id)
	end

end
