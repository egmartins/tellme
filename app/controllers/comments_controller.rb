class CommentsController < ApplicationController
	def create
		@story = Story.find(params[:story_id])
		@comments = @story.comments.create(params[:comment].permit(:name, :body))

		redirect_to story_path(@story) 
	end
end
