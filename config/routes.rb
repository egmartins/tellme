Rails.application.routes.draw do

devise_for :users, :controllers => {registrations: 'registrations'}
resources :users, :only => [:show]

get "tags/:tag", to: "stories#index", as: :tag
get "users/:id", to: "registrations#show" 

resources :stories do
	member do
		get 'like', to: 'stories#upvote'
		get 'dislike', to: 'stories#downvote'
	end
	resources :comments
end

authenticated :user do
	root 'stories#index', as: "authenticated_root"
end

root 'front_pages#index'

end
